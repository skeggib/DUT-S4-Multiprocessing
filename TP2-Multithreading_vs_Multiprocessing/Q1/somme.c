double** somme(double** m1, double** m2, int size) {
	double r[size][size];

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			r[i][j] = m1[i][j] + m2[i][j];
		}
	}

	return r;
}
