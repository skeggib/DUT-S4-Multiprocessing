double** produit(double** m1, double** m2, int size) {
	double r[size][size];

	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			for (int k = 0; k < size; k++) {
				r[i][j] = m1[i][k] * m2[k][j];
			}
		}
	}

	return r;
}
