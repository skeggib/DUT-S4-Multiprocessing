/*
 * This is sample code generated by rpcgen.
 * These are only templates and you can use them
 * as a guideline for developing your own functions.
 */

#include "testrpc.h"


void
progservice_1(char *host, int x1, int x2, char command)
{
	CLIENT *clnt;

	entree	arg;
	arg.x1 = x1;
	arg.x2 = x2;

	int  *result_1;
	int  *result_2;
	entree  *result_3;
	void  *result_4;

#ifndef	DEBUG
	clnt = clnt_create (host, PROGSERVICE, VERSERVICE, "udp");
	if (clnt == NULL) {
		clnt_pcreateerror (host);
		exit (1);
	}
#endif	/* DEBUG */

	if (command == 's') {
		result_1 = masomme_1(&arg, clnt);
		if (result_1 == (int *) NULL) {
			clnt_perror (clnt, "call failed");
		}
		else
			printf("Somme : [%d; %d] => %d\n", arg.x1, arg.x2, *result_1);
	}

	else if (command == 'p') {
		result_2 = monproduit_1(&arg, clnt);
		if (result_2 == (int *) NULL) {
			clnt_perror (clnt, "call failed");
		}
		else
			printf("Produit : [%d; %d] => %d\n", arg.x1, arg.x2, *result_2);
	}

	else if (command == 't') {
		result_3 = transpose_1(&arg, clnt);
		if (result_3 == (entree *) NULL) {
			clnt_perror (clnt, "call failed");
		}
		else
			printf("Transpose : [%d; %d] => [%d; %d]\n", arg.x1, arg.x2, result_3->x1, result_3->x2);
	}

	else if (command == 'a') {
		arret_1((void*)&arg, clnt);
		if (result_4 == (void *) NULL) {
			clnt_perror (clnt, "call failed");
		}
	}

#ifndef	DEBUG
	clnt_destroy (clnt);
#endif	 /* DEBUG */
}


int
main (int argc, char *argv[])
{
	char *host;
	char command;
	int x1;
	int x2;

	if (argc < 5) {
		printf ("usage: %s server_host x1 x2 command(char)\n", argv[0]);
		exit (1);
	}
	host = argv[1];
	x1 = atoi(argv[2]);
	x2 = atoi(argv[3]);
	command = argv[4][0];

	printf("host: %s\tx1: %d\tx2: %d\tcommand: %c\n", host, x1, x2, command);

	progservice_1 (host, x1, x2, command);
exit (0);
}
