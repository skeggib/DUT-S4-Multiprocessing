struct entree {
	int x1;
	int x2;
};

program PROGSERVICE {
	version VERSERVICE {
		int masomme(entree) = 1;
		int monproduit(entree) = 2;
		entree transpose(entree) = 3;
		void arret(void) = 4;
	} = 1;
} = 2000001;
