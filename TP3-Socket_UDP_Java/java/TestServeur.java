import java.io.*;
import java.net.*;

public class TestServeur {
	public static void main(String[] args) {
		
		if (args.length != 1) {
			System.out.println("Usage : java TestServeur <port>");
			System.exit(0);
		}

		try {
			System.out.println("*** Demarrage du serveur ***");

			ClientServeurUDP serveur = new ClientServeurUDP(Integer.parseInt(args[0]));

			String message = "";
			while (true) {
				message = serveur.recevoirMessage();
				String ip = serveur.getAdresseIPCorrespondant();
				System.out.println(ip + " : '" + message + "'");
				serveur.envoyerMessage("Message recu : " + message + "'");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}

	}
}
