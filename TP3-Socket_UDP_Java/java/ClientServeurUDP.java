import java.net.*;
import java.io.*;
    
public class ClientServeurUDP
{ 
	// Variables d'instance :
	private DatagramSocket  socket;
	private InetAddress  adresseIPCorrespondant;
	private int    port;

	// Constructeur pour le client :
	public ClientServeurUDP(String adresseDest,int portDest) throws IOException
	{
		this.socket = new DatagramSocket();
		this.adresseIPCorrespondant = InetAddress.getByName(adresseDest);
		this.port = portDest;
	}

	// Constructeur pour le serveur :
	public ClientServeurUDP(int portEcoute) throws IOException
	{
		this.socket = new DatagramSocket(portEcoute);
		this.port = portEcoute;
	}

	// Méthodes :
	public void envoyerMessage(String mesg)  throws IOException
	{
		byte[] data = new byte[1024];
		data = mesg.getBytes();
		DatagramPacket packet = new DatagramPacket(data, data.length,
			this.adresseIPCorrespondant, this.port);
		this.socket.send(packet);
	}

	public String recevoirMessage() throws IOException
	{
		byte[] data = new byte[1024];
		DatagramPacket packet = new DatagramPacket(data, data.length);
		this.socket.receive(packet);
		this.adresseIPCorrespondant = packet.getAddress();
		this.port = packet.getPort();
		return new String(packet.getData());
	}

	public String getAdresseIPCorrespondant() {
		return this.adresseIPCorrespondant.getHostAddress();
	}

	public void delaiMax(int delai) throws SocketException
	{
		this.socket.setSoTimeout(delai);
	}
}
