# 2.1 Questions préliminaires

## Q.1 : Quel est le mode de communication (connecté ou non) ? Quelle classe Java va être utilisée pour créer les sockets ?

Nous allons travailler en UDP, en mode non-connecté donc.
Nous allons utiliser la classe DatagramSocket.

## Q.2 : A quoi sert le fichier `/etc/services` ? Regardez les ports 7, 13 et 80.

Ce fichier associe les services et les ports.

Le port 7 est reservé au service `echo`.
Le port 13 est reservé au service `daytime`.
Le port 80 est reservé au service `http`.

## Q.3 : Quelle est l'adresse symbolique de votre machine ?

`HAL9000`

## Q.4 : Quelle est l'adresse IP de votre machine ? L'adresse MAC de sa première carte Ethernet ? L'adresse de diffusion ?

IP : `10.100.20.88`
MAC : a0:a8:ce:7a:66:32
Adresse de diffusion : `10.100.31.255`

## Q.5 : La table de route de votre machine permet-elle de sortir de la salle ? De l'IUT ?

Oui, la passerelle par defaut est `10.100.31.254`.

## Q.6 : Quels sont les ports TCP actuellement ouverts sur votre poste ?

`netstat --tcp`

- 55418
- 44772
- 37820
- 39036
- 44744
- ...

## Q.7 : Quels sont les ports UDP actuellement ouverts sur votre poste ?

`netstat --udp`

- 48512
- 48617
- 55325
- 37215
